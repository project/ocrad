
Current state of Ocrad for Drupal 7
-------------------------------------

The ocrad drupal module is able to import single files, such as
.pbm, .ppm and .pgm, and display the result recognizing the text.

Installation
------------

It needs ocrad unix command installed in the system if you are using a
system based in debian, such as trisquel or ubuntu, you can do:

apt-get install ocrad.

Ocrad can be installed like any other Drupal module -- place it in
the modules directory for your site and enable it on the
`admin/modules` page, or with drush.

Using
-----

Currently, you can upload image files from /ocrad with a single form.

Maintainer
-----------

- davidam (David Arroyo Menéndez)
